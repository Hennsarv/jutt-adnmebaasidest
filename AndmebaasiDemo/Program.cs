﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace AndmebaasiDemo
{
    partial class Employee
    {
        // readonly property classis Employee
        public string FullName => $"{FirstName} {LastName}"; 
    }

    partial class Category
    {
        public int ProductCount => Products.Count;
    }

    class Program
    {
        static void Main(string[] args)
        {
            // connection - ühendus andmebaasiga
            // command - küsimus, mille ma saadan andmebaasi
            // recordset - datareader - vastus, mille ma sealt saan
            if (false)
            {
                string connectionString = "Data Source=valiit.database.windows.net;Initial Catalog=Northwind;User ID=student;Password=Pa$$w0rd";
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                SqlCommand comm = new SqlCommand("select * from products", conn);
                var reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader[1]} hind on {reader[5]}");
                }

            }

            NorthwindEntities ne = new NorthwindEntities();
            //foreach(var cat in ne.Categories)
            //{
            //    Console.WriteLine($"{cat.CategoryName} tooted on:");
            //    foreach(var prod in cat.Products)
            //        Console.WriteLine($"\t{prod.ProductName}");



            //}

            

            foreach(Employee emp in ne.Employees)
                Console.WriteLine(emp.FullName);
            foreach(var cat in ne.Categories)
                Console.WriteLine($"kategoorias {cat.CategoryName} on {cat.ProductCount} toodet" );

            var Töötajad = ne.Employees.ToList();

        }

    }
}
